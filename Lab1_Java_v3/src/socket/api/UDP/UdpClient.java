package socket.api.UDP;

import java.io.*;
import java.net.*;
import java.util.Scanner;

public class UdpClient {
	@SuppressWarnings("unused")
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		try {
			sentence_sending: {
			DatagramSocket s = new DatagramSocket();
			InetAddress IPAddress = InetAddress.getByName("localhost");//
				String sentence = scan.nextLine();
				if (sentence.split(" ")[0].equals("send")) {
					try {
						String filename = sentence.split(" ")[1];
						byte[] data = new byte[1000];
						FileInputStream fr = new FileInputStream(new File(filename));
						DatagramPacket pac;

						byte[] signal = ("_sending " + filename).getBytes();

						int num_of_pack = 0;
						pac = new DatagramPacket(signal, signal.length, IPAddress, 8033);
						s.send(pac);
						while (fr.available() > 0) {
							fr.read(data);
							pac = new DatagramPacket(data, data.length, IPAddress, 8033);
							s.send(pac);
							System.out.printf("Pasket � %d was sent\n", num_of_pack++);
						}
						fr.close();
						System.out.println("File transffered");
						new UdpClient().main(null);
						break sentence_sending;

					} catch (Exception e) {
						e.printStackTrace();

					}
				} else {
					try {
						while (!sentence.equals("stop")&&(!s.isClosed())) {
							byte[] sendData = new byte[1024];//
							byte[] receiveData = new byte[1024];
							sendData = sentence.getBytes();
							DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, IPAddress, 8033);
							s.send(sendPacket);
							DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
							s.receive(receivePacket);
							String modifiedSentence = new String(receivePacket.getData());
							System.out.println("FROM SERVER:" + modifiedSentence);
							s.close();
							new UdpClient().main(null);
						}
						break sentence_sending;
					} catch (Exception e) {
						e.printStackTrace();

					}
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}