package socket.api.UDP;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.nio.channels.DatagramChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class UdpServer {
	public static void main(String[] args) {
		// File file = new File("toxic2.mp3");
		//System.out.println("Initialized");
		try {
			acceptFile(8033, 1000);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@SuppressWarnings("unused")
	private static void acceptFile(int port, int pacSize) throws IOException {
		sentense_getting: {
			byte data[] = new byte[pacSize];
			DatagramPacket pac = new DatagramPacket(data, data.length);
			DatagramSocket s = new DatagramSocket(port);
			Scanner scan = new Scanner(System.in);
			s.receive(pac);
			String sentence = new String(pac.getData());
			if (sentence.startsWith("_sending")) {
				sending: {
					String filename = sentence.split(" ")[1].split("/")[1];
					filename = filename.trim();
					File file = new File(filename);
					file.createNewFile();
					FileOutputStream os = new FileOutputStream(file);

					try {
						s.setSoTimeout(30000);
						while (true) {
							s.receive(pac);
					//		System.out.println(".");
							os.write(data);
							os.flush();
						}

					} catch (Exception e) {
						os.close();
						s.close();
						new UdpServer().main(null);
						System.out.println("Ready to receive");
						break sending;
					}
				}
			} else {
				System.out.println(sentence);
				byte[] message = scan.nextLine().getBytes();
				DatagramPacket packet = new DatagramPacket(message, message.length, pac.getAddress(), pac.getPort());
				s.send(packet);
				s.close();
				new UdpServer().main(null);
				break sentense_getting;
			}
		}
	}
}
