package sockets.api.general.ssl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.Scanner;

import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;

public class SslClient {

	public static final boolean DEBUG = true;
	public static final int HTTPS_PORT = 8282;
	public static final String HTTPS_HOST = "localhost";
	public static final String TRUSTTORE_LOCATION = "C:/Keys/DebKeyStore.jks";


	public static void main(String[] args) {

		System.setProperty("javax.net.ssl.trustStore", TRUSTTORE_LOCATION);
		System.setProperty("javax.net.ssl.trustStorePassword", "zxcvbnm1");

		if (DEBUG)
			System.setProperty("javax.net.debug", "ssl:record");

		SSLSocketFactory f = (SSLSocketFactory) SSLSocketFactory.getDefault();
		try {
			SSLSocket c = (SSLSocket) f.createSocket(HTTPS_HOST, HTTPS_PORT);

			c.startHandshake();
			
			BufferedReader in=new BufferedReader(new InputStreamReader(c.getInputStream()));
			PrintWriter out = new PrintWriter(c.getOutputStream(),true);
			
			Scanner scan=new Scanner(System.in);
			String output="";
			new InputWatcher1(in).start();
			while(!output.equals("_stop")){
				output=scan.nextLine();
				out.println(output);
				out.flush();			
			}
	} catch (IOException e) {
			 System.err.println(e.toString());
	}

	}
}
