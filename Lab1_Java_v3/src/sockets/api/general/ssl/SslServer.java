package sockets.api.general.ssl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

import javax.net.ServerSocketFactory;
import javax.net.ssl.SSLServerSocket;
import javax.net.ssl.SSLServerSocketFactory;

public class SslServer {
	public static final boolean DEBUG = true;
	public static final int HTTPS_PORT = 8282;
	public static final String KEYSTORE_LOCATION = "C:/Keys/ServerKeyStore.jks";
	public static final String KEYSTORE_PASSWORD = "zxcvbnm1";

	public static void main(String argv[]) throws Exception {

		System.setProperty("javax.net.ssl.keyStore", KEYSTORE_LOCATION);
		System.setProperty("javax.net.ssl.keyStorePassword", KEYSTORE_PASSWORD);

		if (DEBUG)
			System.setProperty("javax.net.debug", "ssl:record");

		SslServer server = new SslServer();
		server.startServer();
	}

	public void startServer() {
		try {
			ServerSocketFactory ssf = (SSLServerSocketFactory) SSLServerSocketFactory.getDefault();
			SSLServerSocket serversocket = (SSLServerSocket) ssf.createServerSocket(HTTPS_PORT);

			while (true) {
				Socket client = serversocket.accept();

				BufferedReader in = new BufferedReader(new InputStreamReader(client.getInputStream()));
				PrintWriter out = new PrintWriter(client.getOutputStream(), true);

				Scanner scan = new Scanner(System.in);
				String output = "";
				new InputWatcher1(in).start();
				while (!output.equals("_stop")) {
					output = scan.nextLine();
					out.println(output);
					out.flush();
				}
			}
		} catch (Exception e) {
			System.out.println("Exception:" + e.getMessage());
		}
	}
}

class InputWatcher1 extends Thread {
	BufferedReader input;

	public InputWatcher1(BufferedReader in) {
		input = in;
	}

	public void run() {
		while (true) {
			try {
				System.out.println(input.readLine());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}